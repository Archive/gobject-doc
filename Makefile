#######################################
#  directory configuration
#######################################
IMG_DIR=img
HTML_DIR=html
PDF_DIR=pdf
CODE_DIR=tree

######################################
#  output files
######################################

OUT_HTML_FILE=gobject-book
OUT_PDF_A4_FILE=gobject-book-a4
OUT_PDF_LETTER_FILE=gobject-book-letter

#######################################
#  input files
#######################################
MAIN_XML_FILE=main.xml
XML_FILES= \
	$(MAIN_XML_FILE) \
	intro.xml \
	gtype.xml \
	gsignal.xml \
	gobject.xml \
	howto.xml \
	howto-hacker.xml \
	changes.xml \
	performance.xml \
	tools.xml

XSL_HTML_FILE=$(if $(XSL_HTML), $(XSL_HTML), html.xsl)
XSL_PDF_FILE=fo.xsl
IMG_FILES= \
	glue

EXTRA_FILES=\
	main.dtd \
	inc.doc \
	Makefile \
	README \
	TODO

SUBDIRS=\
	sample/gtype \
	sample/gobject \
	sample/interface
#	sample/tree


#######################################
#  version number
#######################################
PACKAGE_NAME=gobject
XML_REQUIRED_VERSION=2.4.5
XSL_REQUIRED_VERSION=1.0.3
DIA_REQUIRED_VERSION=0.90
VERSION=0.10.0

include ./inc.doc





/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-subbar.h"

int main (int argc, char **argv)
{
        GObject *bar;
        char *value;
        guchar papa;

        g_type_init ();

        //bar = g_object_new (MAMAN_TYPE_BAR, NULL);
        //bar = g_object_new (MAMAN_TYPE_SUBBAR, NULL);
        bar = g_object_new (MAMAN_TYPE_SUBBAR, NULL);
        
        maman_bar_do_action_public (MAMAN_BAR (bar), 0);

        maman_bar_do_action_public_virtual (MAMAN_BAR (bar), 0);

        maman_bar_do_action_public_pure_virtual (MAMAN_BAR (bar), 0);

        /* test properties. */
        g_object_get (G_OBJECT (bar), "maman-name", &value, NULL);
        g_assert (strcmp (value, "no-name-set") == 0);
        /*
          Cannot change the name of maman since it is a construct-only property.
          g_object_set (G_OBJECT (bar), "maman-name", "martine");
        */
        g_object_set (G_OBJECT (bar), "papa-number", 1, NULL);
        g_object_get (G_OBJECT (bar), "papa-number", &papa, NULL);
        g_assert (papa == 1);

        g_object_unref (G_OBJECT (bar));


        /* Here, we set the maman-name construction property. */
        bar = g_object_new (MAMAN_TYPE_SUBBAR, "maman-name", "martine", NULL);
        g_object_get (G_OBJECT (bar), "maman-name", &value, NULL);
        g_assert (strcmp (value, "martine") == 0);

        g_object_unref (G_OBJECT (bar));

        return 0;
}

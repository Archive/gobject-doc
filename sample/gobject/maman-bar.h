/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/*
 * Copyright/Licensing information.
 */

#ifndef MAMAN_BAR_H
#define MAMAN_BAR_H

/*
 * Potentially, include other headers on which this header depends.
 */
#include <glib-object.h>

#define MAMAN_TYPE_BAR		  (maman_bar_get_type ())
#define MAMAN_BAR(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_TYPE_BAR, MamanBar))
#define MAMAN_BAR_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), MAMAN_TYPE_BAR, MamanBarClass))
#define MAMAN_IS_BAR(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_TYPE_BAR))
#define MAMAN_IS_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MAMAN_TYPE_BAR))
#define MAMAN_BAR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAMAN_TYPE_BAR, MamanBarClass))

typedef struct _MamanBar MamanBar;
typedef struct _MamanBarClass MamanBarClass;
typedef struct _MamanBarPrivate MamanBarPrivate;

struct _MamanBar {
	GObject parent;
	/* instance members */
        MamanBarPrivate *private;
};

struct _MamanBarClass {
	GObjectClass parent;

	/* class members */
        void (*do_action_public_virtual) (MamanBar *self, guint8 i);

        void (*do_action_public_pure_virtual) (MamanBar *self, guint8 i);

        void (*do_action_private_virtual) (MamanBar *self, guint8 i);

        void (*do_action_private_pure_virtual) (MamanBar *self, guint8 i);
};

/* used by MAMAN_TYPE_BAR */
GType maman_bar_get_type (void);

/* API. */

void maman_bar_do_action_public (MamanBar *self, guint8 i);

void maman_bar_do_action_public_virtual (MamanBar *self, guint8 i);

void maman_bar_do_action_public_pure_virtual (MamanBar *self, guint8 i);

#endif /* MAMAN_BAR_H */




/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/*
 * Copyright/Licensing information.
 */

#ifndef MAMAN_SUBBAR_H
#define MAMAN_SUBBAR_H

/*
 * Potentially, include other headers on which this header depends.
 */
#include <glib-object.h>
#include "maman-bar.h"

#define MAMAN_TYPE_SUBBAR		  (maman_subbar_get_type ())
#define MAMAN_SUBBAR(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_TYPE_SUBBAR, MamanSubbar))
#define MAMAN_SUBBAR_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), MAMAN_TYPE_SUBBAR, MamanSubbarClass))
#define MAMAN_IS_SUBBAR(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_TYPE_SUBBAR))
#define MAMAN_IS_SUBBAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MAMAN_TYPE_SUBBAR))
#define MAMAN_SUBBAR_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAMAN_TYPE_SUBBAR, MamanSubbarClass))

typedef struct _MamanSubBar MamanSubBar;
typedef struct _MamanSubBarClass MamanSubBarClass;

struct _MamanSubBar {
        MamanBar parent;
};

struct _MamanSubBarClass {
        MamanBarClass parent;
};

/* used by MAMAN_TYPE_SUBBAR */
GType maman_subbar_get_type (void);

#endif /* MAMAN_SUBBAR_H */




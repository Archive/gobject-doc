/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-subbar.h"


static void 
maman_subbar_real_do_action_public_pure_virtual (MamanBar *obj, guint8 i)
{
        MamanSubBar *self = (MamanSubBar *) obj;
        /* Real job is done here. */
}

static void 
maman_subbar_real_do_action_private_pure_virtual (MamanBar *obj, guint8 i)
{
        MamanSubBar *self = (MamanSubBar *) obj;
        /* Real job is done here. */
}

static void
maman_subbar_class_init (gpointer g_class,
			 gpointer g_class_data)
{
        MamanBarClass *klass = MAMAN_BAR_CLASS (g_class);

        klass->do_action_public_pure_virtual = maman_subbar_real_do_action_public_pure_virtual;
        klass->do_action_private_pure_virtual = maman_subbar_real_do_action_private_pure_virtual;
}

GType maman_subbar_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanSubBarClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        maman_subbar_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanSubBar),
                        0,      /* n_preallocs */
                        NULL    /* instance_init */
                };
                type = g_type_register_static (MAMAN_TYPE_BAR,
                                               "MamanSubBarType",
                                               &info, 0);
        }
        return type;
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-bar.h"

/* If you use Pimpls, include the private structure 
 * definition here. Some people create a maman-bar-private.h header
 * which is included by the maman-bar.c file and which contains the
 * definition for this private structure.
 */

struct _MamanBarPrivate
{
        guint8 useless;
        gboolean dispose_has_run;
        gchar *name;
        guchar papa_number;
};

static GObjectClass *parent_class = NULL;

void maman_bar_do_action_public_virtual (MamanBar *self, guint8 i)
{
        if (self->private->dispose_has_run) {
                return;
        }
        MAMAN_BAR_GET_CLASS (self)->do_action_public_virtual (self, i);
}

void maman_bar_do_action_public_pure_virtual (MamanBar *self, guint8 i)
{
        if (self->private->dispose_has_run) {
                return;
        }
        MAMAN_BAR_GET_CLASS (self)->do_action_public_pure_virtual (self, i);
}

static void maman_bar_do_action_private_virtual (MamanBar *self, guint8 i)
{
        MAMAN_BAR_GET_CLASS (self)->do_action_private_virtual (self, i);
}

static void maman_bar_do_action_private_pure_virtual (MamanBar *self, guint8 i)
{
        MAMAN_BAR_GET_CLASS (self)->do_action_private_pure_virtual (self, i);
}

static void 
real_do_action_public_virtual (MamanBar *self, guint8 i)
{
        /* a default implementation for this class method */
}

static void 
real_do_action_private_virtual (MamanBar *self, guint8 i)
{
        /* a default implementation for this class method */
}


void maman_bar_do_action_public (MamanBar *self, guint8 i)
{
        if (self->private->dispose_has_run) {
                return;
        }

        /* Do something useless on MamanBar instance. */
        self->private->useless = i;

        /* call helper function implemented by children */
        maman_bar_do_action_private_virtual (self, 1);

        /* call helper function implemented by children */
        maman_bar_do_action_private_pure_virtual (self, 2);
}

enum {
        MAMAN_BAR_CONSTRUCT_NAME = 1,
        MAMAN_BAR_PAPA_NUMBER,
};

static void
maman_bar_instance_init (GTypeInstance   *instance,
                         gpointer         g_class)
{
        MamanBar *self = (MamanBar *)instance;
        self->private = g_new (MamanBarPrivate, 1);
        self->private->useless = 0;
        self->private->dispose_has_run = FALSE;
        self->private->name = NULL;
        self->private->papa_number = 0;
}


static void
maman_bar_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
        MamanBar *self = (MamanBar *) object;

        switch (property_id) {
        case MAMAN_BAR_CONSTRUCT_NAME: {
                g_free (self->private->name);
                self->private->name = g_value_dup_string (value);
                g_print ("maman: %s\n",self->private->name);
        }
                break;
        case MAMAN_BAR_PAPA_NUMBER: {
                self->private->papa_number = g_value_get_uchar (value);
                g_print ("papa: %u\n",self->private->papa_number);
        }
                break;
        default:
                /* We don't have any other property... */
                G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
                break;
        }
}

static void
maman_bar_get_property (GObject      *object,
                        guint         property_id,
                        GValue       *value,
                        GParamSpec   *pspec)
{
        MamanBar *self = (MamanBar *) object;

        switch (property_id) {
        case MAMAN_BAR_CONSTRUCT_NAME: {
                g_value_set_string (value, self->private->name);
        }
                break;
        case MAMAN_BAR_PAPA_NUMBER: {
                g_value_set_uchar (value, self->private->papa_number);
        }
                break;
        default:
                /* We don't have any other property... */
                G_OBJECT_WARN_INVALID_PROPERTY_ID(object,property_id,pspec);
                break;
        }
}

static GObject *
maman_bar_constructor (GType                  type,
                       guint                  n_construct_properties,
                       GObjectConstructParam *construct_properties)
{
        GObject *obj;

        {
                /* Invoke parent constructor. */
                MamanBarClass *klass;
                klass = MAMAN_BAR_CLASS (g_type_class_peek (MAMAN_TYPE_BAR));
                obj = parent_class->constructor (type,
                                                 n_construct_properties,
                                                 construct_properties);
        }
        
        /* do stuff. */

        return obj;
}

static void
maman_bar_dispose (GObject *obj)
{
        MamanBar *self = (MamanBar *)obj;

        if (self->private->dispose_has_run) {
                /* If dispose did already run, return. */
                return;
        }
        /* Make sure dispose does not run twice. */
        self->private->dispose_has_run = TRUE;

        /* 
         * In dispose, you are supposed to free all types referenced from this
         * object which might themselves hold a reference to self. Generally,
         * the most simple solution is to unref all members on which you own a 
         * reference.
         */

        /* Chain up to the parent class */
        G_OBJECT_CLASS (parent_class)->dispose (obj);
}

static void
maman_bar_finalize (GObject *obj)
{
        MamanBar *self = (MamanBar *)obj;

        /*
         * Here, complete object destruction.
         * You might not need to do much...
         */
        g_free (self->private->name);
        
        g_free (self->private);

        /* Chain up to the parent class */
        G_OBJECT_CLASS (parent_class)->finalize (obj);
}


static void
maman_bar_class_init (gpointer g_class,
                      gpointer g_class_data)
{
        GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
        MamanBarClass *klass = MAMAN_BAR_CLASS (g_class);
        GParamSpec *pspec;

        gobject_class->set_property = maman_bar_set_property;
        gobject_class->get_property = maman_bar_get_property;
        gobject_class->dispose = maman_bar_dispose;
        gobject_class->finalize = maman_bar_finalize;
        gobject_class->constructor = maman_bar_constructor;

        klass->do_action_public_pure_virtual = NULL;
        klass->do_action_public_virtual = real_do_action_public_virtual;
        klass->do_action_private_pure_virtual = NULL;
        klass->do_action_private_virtual = real_do_action_private_virtual;

	parent_class = g_type_class_peek_parent (klass);

        pspec = g_param_spec_string ("maman-name",
                                     "Maman construct prop",
                                     "Set maman's name",
                                     "no-name-set" /* default value */,
                                     G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);
        g_object_class_install_property (gobject_class,
                                         MAMAN_BAR_CONSTRUCT_NAME,
                                         pspec);

        pspec = g_param_spec_uchar ("papa-number",
                                    "Number of current Papa",
                                    "Set/Get papa's number",
                                    0  /* minimum value */,
                                    10 /* maximum value */,
                                    2  /* default value */,
                                    G_PARAM_READWRITE);
        g_object_class_install_property (gobject_class,
                                         MAMAN_BAR_PAPA_NUMBER,
                                         pspec);
}

GType maman_bar_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanBarClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        maman_bar_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanBar),
                        0,      /* n_preallocs */
                        maman_bar_instance_init    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanBarType",
                                               &info, 0);
        }
        return type;
}

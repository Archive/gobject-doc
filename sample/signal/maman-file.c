/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-file.h"

struct _MamanFilePrivate
{
};

static void
maman_file_instance_init (GTypeInstance   *instance,
                         gpointer         g_class)
{
        MamanFile *self = (MamanFile *)instance;
        self->private = g_new (MamanFilePrivate, 1);
}

static void
maman_file_class_init (gpointer g_class,
                      gpointer g_class_data)
{
        GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
        MamanFileClass *klass = MAMAN_FILE_CLASS (g_class);

        klass->write_signal_id = 
                g_signal_newv ("write",
                               G_TYPE_FROM_CLASS (g_class),
                               G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                               NULL /* class closure */,
                               NULL /* accumulator */,
                               NULL /* accu_data */,
                               g_cclosure_marshal_VOID__VOID,
                               G_TYPE_NONE /* return_type */,
                               0     /* n_params */,
                               NULL  /* param_types */);

}

GType maman_file_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanFileClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        maman_file_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanFile),
                        0,      /* n_preallocs */
                        maman_file_instance_init    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanFileType",
                                               &info, 0);
        }
        return type;
}

void maman_file_write (MamanFile *self, guint8 *buffer, guint32 size)
{
        /* First write data. */
        /* Then, notify user of data written. */
        g_signal_emit (self, MAMAN_FILE_GET_CLASS (self)->write_signal_id,
                       0 /* details */, 
                       NULL);
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-file.h"
#include "maman-file-complex.h"
#include "maman-file-simple.h"

/******
Test File Object
******/

static void
write_event (GObject *obj, gpointer user_data)
{
        g_assert (user_data == NULL);
        g_print ("Write event\n");
}

static void test_file (void)
{
	guint8 buffer[100];
        GObject *file;

        file = g_object_new (MAMAN_TYPE_FILE, NULL);

        g_signal_connect (G_OBJECT (file), "write",
                          (GCallback)write_event,
                          NULL);

        g_signal_connect (G_OBJECT (file), "write",
                          (GCallback)write_event,
                          NULL);

        g_signal_connect (G_OBJECT (file), "write",
                          (GCallback)write_event,
                          NULL);

	maman_file_write (MAMAN_FILE (file), buffer, 50);

        g_object_unref (G_OBJECT (file));
}


/******
Test Complex File Object
******/


static void complex_write_event_before (GObject *file, guint8 *buffer, guint size, gpointer user_data)
{
        g_assert (user_data == NULL);
        g_print ("Complex Write event before: 0x%x, %u\n", buffer, size);
}

static void complex_write_event_after (GObject *file, guint8 *buffer, guint size, gpointer user_data)
{
        g_assert (user_data == NULL);
        g_print ("Complex Write event after: 0x%x, %u\n", buffer, size);
}

static void test_file_complex (void)
{
	guint8 buffer[100];
        GObject *file;

        file = g_object_new (MAMAN_FILE_COMPLEX_TYPE, NULL);

        g_signal_connect (G_OBJECT (file), "write",
                          (GCallback)complex_write_event_before,
                          NULL);

        g_signal_connect_after (G_OBJECT (file), "write",
                                (GCallback)complex_write_event_after,
                                NULL);

	maman_file_complex_write (MAMAN_FILE_COMPLEX (file), buffer, 50);

        g_object_unref (G_OBJECT (file));
}


/******
Test Simple File Object
******/


static void simple_write_event_before (GObject *file, guint8 *buffer, guint size, gpointer user_data)
{
        g_assert (user_data == NULL);
        g_print ("Simple Write event before: 0x%x, %u\n", buffer, size);
}

static void simple_write_event_after (GObject *file, guint8 *buffer, guint size, gpointer user_data)
{
        g_assert (user_data == NULL);
        g_print ("Simple Write event after: 0x%x, %u\n", buffer, size);
}

static void test_file_simple (void)
{
	guint8 buffer[100];
        GObject *file;

        file = g_object_new (MAMAN_FILE_SIMPLE_TYPE, NULL);

        g_signal_connect (G_OBJECT (file), "write",
                          (GCallback)simple_write_event_before,
                          NULL);

        g_signal_connect_after (G_OBJECT (file), "write",
                                (GCallback)simple_write_event_after,
                                NULL);

	maman_file_simple_write (MAMAN_FILE_SIMPLE (file), buffer, 50);

        g_object_unref (G_OBJECT (file));
}


int main (int argc, char **argv)
{
        g_type_init ();

        test_file ();

        test_file_complex ();

        test_file_simple ();

        return 0;
}

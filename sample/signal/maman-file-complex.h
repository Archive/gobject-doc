/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#ifndef MAMAN_FILE_COMPLEX_H
#define MAMAN_FILE_COMPLEX_H

/*
 * Potentially, include other headers on which this header depends.
 */
#include <glib-object.h>

#define MAMAN_FILE_COMPLEX_TYPE		  (maman_file_complex_get_type ())
#define MAMAN_FILE_COMPLEX(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_FILE_COMPLEX_TYPE, MamanFileComplex))
#define MAMAN_FILE_COMPLEX_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), MAMAN_FILE_COMPLEX_TYPE, MamanFileComplexClass))
#define MAMAN_IS_FILE_COMPLEX(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_FILE_COMPLEX_TYPE))
#define MAMAN_IS_FILE_COMPLEX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MAMAN_FILE_COMPLEX_TYPE))
#define MAMAN_FILE_COMPLEX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAMAN_FILE_COMPLEX_TYPE, MamanFileComplexClass))

typedef struct _MamanFileComplex MamanFileComplex;
typedef struct _MamanFileComplexClass MamanFileComplexClass;
typedef struct _MamanFileComplexPrivate MamanFileComplexPrivate;

struct _MamanFileComplex {
	GObject parent;
	/* instance members */
        MamanFileComplexPrivate *private;
};

struct _MamanFileComplexClass {
	GObjectClass parent;
        
        guint write_signal_id;
};

/* used by MAMAN_FILE_COMPLEX_TYPE */
GType maman_file_complex_get_type (void);

/* API. */

void maman_file_complex_write (MamanFileComplex *self, guint8 *buffer, guint size);


#endif /* MAMAN_FILE_COMPLEX_H */

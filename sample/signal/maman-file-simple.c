/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-file-simple.h"

#include <glib-object.h>

#include "maman-file-complex-marshall.h"

struct _MamanFileSimplePrivate
{
};

static void
maman_file_simple_instance_init (GTypeInstance   *instance,
                                  gpointer         g_class)
{
        MamanFileSimple *self = (MamanFileSimple *)instance;
        self->private = g_new (MamanFileSimplePrivate, 1);
}


static void
default_write_signal_handler (MamanFileSimple *obj, guint8 *buffer, guint size)
{
        /* Here, we trigger the real file write. */
        g_print ("default signal handler: 0x%x %u\n", buffer, size);
}

static void
maman_file_simple_class_init (gpointer g_class,
                               gpointer g_class_data)
{
        GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
        MamanFileSimpleClass *klass = MAMAN_FILE_SIMPLE_CLASS (g_class);

        klass->write = default_write_signal_handler;

        klass->write_signal_id = 
                g_signal_new ("write",
                              G_TYPE_FROM_CLASS (g_class),
                              G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                              G_STRUCT_OFFSET (MamanFileSimpleClass, write),
                              NULL /* accumulator */,
                              NULL /* accu_data */,
                              maman_file_complex_VOID__POINTER_UINT,
                              G_TYPE_NONE /* return_type */,
                              2     /* n_params */,
                              G_TYPE_POINTER,
                              G_TYPE_UINT);
}

GType maman_file_simple_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanFileSimpleClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        maman_file_simple_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanFileSimple),
                        0,      /* n_preallocs */
                        maman_file_simple_instance_init    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanFileSimpleType",
                                               &info, 0);
        }
        return type;
}

void maman_file_simple_write (MamanFileSimple *self, guint8 *buffer, guint size)
{
        /* trigger event */
        g_signal_emit (self,
                       MAMAN_FILE_SIMPLE_GET_CLASS (self)->write_signal_id,
                       0, /* details */
                       buffer, size);
}

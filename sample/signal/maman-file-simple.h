/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#ifndef MAMAN_FILE_SIMPLE_H
#define MAMAN_FILE_SIMPLE_H

/*
 * Potentially, include other headers on which this header depends.
 */
#include <glib-object.h>

#define MAMAN_FILE_SIMPLE_TYPE		  (maman_file_simple_get_type ())
#define MAMAN_FILE_SIMPLE(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_FILE_SIMPLE_TYPE, MamanFileSimple))
#define MAMAN_FILE_SIMPLE_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), MAMAN_FILE_SIMPLE_TYPE, MamanFileSimpleClass))
#define MAMAN_IS_FILE_SIMPLE(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_FILE_SIMPLE_TYPE))
#define MAMAN_IS_FILE_SIMPLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MAMAN_FILE_SIMPLE_TYPE))
#define MAMAN_FILE_SIMPLE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAMAN_FILE_SIMPLE_TYPE, MamanFileSimpleClass))

typedef struct _MamanFileSimple MamanFileSimple;
typedef struct _MamanFileSimpleClass MamanFileSimpleClass;
typedef struct _MamanFileSimplePrivate MamanFileSimplePrivate;

struct _MamanFileSimple {
	GObject parent;
	/* instance members */
        MamanFileSimplePrivate *private;
};

struct _MamanFileSimpleClass {
	GObjectClass parent;
        
        guint write_signal_id;

        /* signal default handlers */
        void (*write) (MamanFileSimple *self, guint8 *buffer, guint size);
};

/* used by MAMAN_FILE_SIMPLE_TYPE */
GType maman_file_simple_get_type (void);

/* API. */

void maman_file_simple_write (MamanFileSimple *self, guint8 *buffer, guint size);


#endif /* MAMAN_FILE_SIMPLE_H */

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-file-complex.h"

#include <glib-object.h>

#include "maman-file-complex-marshall.c"

struct _MamanFileComplexPrivate
{
};

static void
maman_file_complex_instance_init (GTypeInstance   *instance,
                                  gpointer         g_class)
{
        MamanFileComplex *self = (MamanFileComplex *)instance;
        self->private = g_new (MamanFileComplexPrivate, 1);
}


static void
default_write_signal_handler (GObject *obj, guint8 *buffer, guint size, gpointer user_data)
{
        g_assert (user_data == (gpointer)0xdeadbeaf);
        /* Here, we trigger the real file write. */
        g_print ("default signal handler: 0x%x %u\n", buffer, size);
}

static void
maman_file_complex_class_init (gpointer g_class,
                               gpointer g_class_data)
{
        GObjectClass *gobject_class = G_OBJECT_CLASS (g_class);
        MamanFileComplexClass *klass = MAMAN_FILE_COMPLEX_CLASS (g_class);
        GClosure *default_closure;
        GType param_types[2];

        default_closure = g_cclosure_new (G_CALLBACK (default_write_signal_handler),
                                          (gpointer)0xdeadbeaf /* user_data */, 
                                          NULL /* destroy_data */);

        param_types[0] = G_TYPE_POINTER;
        param_types[1] = G_TYPE_UINT;
        klass->write_signal_id = 
                g_signal_newv ("write",
                               G_TYPE_FROM_CLASS (g_class),
                               G_SIGNAL_RUN_LAST | G_SIGNAL_NO_RECURSE | G_SIGNAL_NO_HOOKS,
                               default_closure /* class closure */,
                               NULL /* accumulator */,
                               NULL /* accu_data */,
                               maman_file_complex_VOID__POINTER_UINT,
                               G_TYPE_NONE /* return_type */,
                               2     /* n_params */,
                               param_types /* param_types */);
}

GType maman_file_complex_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanFileComplexClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        maman_file_complex_class_init,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanFileComplex),
                        0,      /* n_preallocs */
                        maman_file_complex_instance_init    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanFileComplexType",
                                               &info, 0);
        }
        return type;
}

void maman_file_complex_write (MamanFileComplex *self, guint8 *buffer, guint size)
{
        /* trigger event */        
        g_signal_emit (self,
                       MAMAN_FILE_COMPLEX_GET_CLASS (self)->write_signal_id,
                       0, /* details */
                       buffer, size);
}

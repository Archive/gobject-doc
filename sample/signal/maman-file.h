/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#ifndef MAMAN_FILE_H
#define MAMAN_FILE_H

/*
 * Potentially, include other headers on which this header depends.
 */
#include <glib-object.h>

#define MAMAN_TYPE_FILE		  (maman_file_get_type ())
#define MAMAN_FILE(obj)		  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_TYPE_FILE, MamanFile))
#define MAMAN_FILE_CLASS(klass)	  (G_TYPE_CHECK_CLASS_CAST ((klass), MAMAN_TYPE_FILE, MamanFileClass))
#define MAMAN_IS_FILE(obj)	  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_TYPE_FILE))
#define MAMAN_IS_FILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MAMAN_TYPE_FILE))
#define MAMAN_FILE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAMAN_TYPE_FILE, MamanFileClass))

typedef struct _MamanFile MamanFile;
typedef struct _MamanFileClass MamanFileClass;
typedef struct _MamanFilePrivate MamanFilePrivate;

struct _MamanFile {
	GObject parent;
	/* instance members */
        MamanFilePrivate *private;
};

struct _MamanFileClass {
	GObjectClass parent;
        
        guint write_signal_id;
};

/* used by MAMAN_TYPE_FILE */
GType maman_file_get_type (void);

/* API. */

void maman_file_write (MamanFile *self, guint8 *buffer, guint32 size);


#endif /* MAMAN_FILE_H */

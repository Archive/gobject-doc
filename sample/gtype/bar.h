typedef struct _MamanBar MamanBar;
typedef struct _MamanBarClass MamanBarClass;

struct _MamanBar {
        GObject parent;
};
struct _MamanBarClass {
        GObjectClass parent;
};

#define MAMAN_BAR_TYPE maman_bar_get_type ()

#include <glib-object.h>

#include "bar.h"

GType maman_bar_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanBarClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        NULL,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanBar),
                        0,      /* n_preallocs */
                        NULL    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanBarType",
                                               &info, 0);
        }
        return type;
}


/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include <glib-object.h>
#include "bar.h"

static void test_int (void)
{
        GValue a_value = {0, }; 
        GValue b_value = {0, };
        guint64 a, b;

        a = 0xdeadbeaf;

        g_value_init (&a_value, G_TYPE_UINT64);
        g_value_set_uint64 (&a_value, a);

        g_value_init (&b_value, G_TYPE_UINT64);
        g_value_copy (&a_value, &b_value);

        b = g_value_get_uint64 (&b_value);

        if (a == b) {
                g_print ("Yay !! 10 lines of code to copy around a uint64.\n");
        } else {
                g_print ("Are you sure this is not a Z80 ?\n");
        }
}


static void test_object (void)
{
        GObject *obj;
        GValue obj_vala = {0, };
        GValue obj_valb = {0, };
        obj = g_object_new (MAMAN_BAR_TYPE, NULL);

        g_value_init (&obj_vala, MAMAN_BAR_TYPE);
        g_value_set_object (&obj_vala, obj);

        g_value_init (&obj_valb, G_TYPE_OBJECT);

        /* g_value_copy's semantics for G_TYPE_OBJECT types is to copy the reference.
           This function thus calls g_object_ref.
           It is interesting to note that the assignment works here because
           MAMAN_BAR_TYPE is a G_TYPE_OBJECT.
         */
        g_value_copy (&obj_vala, &obj_valb);

        g_object_unref (G_OBJECT (obj));
        g_object_unref (G_OBJECT (obj));
}

int main (int argc, char **argv)
{
        g_type_init ();

        test_int ();

        test_object ();
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-baz.h"
#include "maman-ibaz.h"

#include "maman-bar.h"
#include "maman-ibaz.h"
#include "maman-ibar.h"

int main (int argc, char **argv)
{
        GObject *obj;
        g_type_init ();

        obj = g_object_new (MAMAN_TYPE_BAZ, NULL);

        maman_ibaz_do_action (MAMAN_IBAZ (obj));

        g_object_unref (G_OBJECT (obj));


        obj = g_object_new (MAMAN_TYPE_BAR, NULL);

        maman_ibaz_do_action (MAMAN_IBAZ (obj));
        maman_ibar_do_another_action (MAMAN_IBAR (obj));

        g_object_unref (G_OBJECT (obj));
}

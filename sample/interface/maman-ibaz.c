/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-ibaz.h"

static void
maman_ibaz_base_init (gpointer g_class)
{
        static gboolean initialized = FALSE;

        if (!initialized) {
                /* create interface signals here. */
                initialized = TRUE;
        }
}

GType
maman_ibaz_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanIbazClass),
                        maman_ibaz_base_init,   /* base_init */
                        NULL,   /* base_finalize */
                        NULL,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        0,
                        0,      /* n_preallocs */
                        NULL    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_INTERFACE, "MamanIbaz", &info, 0);
        }
        return type;
}


void maman_ibaz_do_action (MamanIbaz *self)
{
        MAMAN_IBAZ_GET_CLASS (self)->do_action (self);
}







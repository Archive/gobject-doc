/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/**
 *
 * MamanBar is a GObject which implements the MamanIBar interface defined in maman-ibar.h file.
 * Since the MamanIBar interface requires the MamanIBaz interface, MamanBar also implements it.
 * 
 */

#ifndef MAMAN_BAR_H
#define MAMAN_BAR_H

#include <glib-object.h>

#define MAMAN_TYPE_BAR             (maman_bar_get_type ())
#define MAMAN_BAR(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_TYPE_BAR, Mamanbar))
#define MAMAN_BAR_CLASS(vtable)    (G_TYPE_CHECK_CLASS_CAST ((vtable), MAMAN_TYPE_BAR, MamanbarClass))
#define MAMAN_IS_BAR(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_TYPE_BAR))
#define MAMAN_IS_BAR_CLASS(vtable) (G_TYPE_CHECK_CLASS_TYPE ((vtable), MAMAN_TYPE_BAR))
#define MAMAN_BAR_GET_CLASS(inst)  (G_TYPE_INSTANCE_GET_CLASS ((inst), MAMAN_TYPE_BAR, MamanbarClass))


typedef struct _MamanBar MamanBar;
typedef struct _MamanBarClass MamanBarClass;

struct _MamanBar {
        GObject parent;
        int instance_member;
};

struct _MamanBarClass {
        GObjectClass parent;
};

GType maman_bar_get_type (void);


#endif //MAMAN_BAR_H

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-ibar.h"
#include "maman-ibaz.h"

static void
maman_ibar_base_init (gpointer g_class)
{
        static gboolean initialized = FALSE;

        if (!initialized) {
                /* create interface signals here. */
                initialized = TRUE;
        }
}

GType
maman_ibar_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanIbarClass),
                        maman_ibar_base_init,   /* base_init */
                        NULL,   /* base_finalize */
                        NULL,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        0,
                        0,      /* n_preallocs */
                        NULL    /* instance_init */
                };
                type = g_type_register_static (G_TYPE_INTERFACE, "MamanIbar", &info, 0);
                /* Make the MamanIbar interface require MamanIbaz interface. */
                g_type_interface_add_prerequisite (type, MAMAN_TYPE_IBAZ);
        }
        return type;
}


void maman_ibar_do_another_action (MamanIbar *self)
{
        MAMAN_IBAR_GET_CLASS (self)->do_another_action (self);
}







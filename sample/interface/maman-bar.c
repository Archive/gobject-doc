/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-bar.h"
#include "maman-ibar.h"
#include "maman-ibaz.h"

static void ibar_do_another_action (MamanBar *self)
{
        g_print ("Bar implementation of IBar interface Another Action: 0x%x.\n", self->instance_member);
}

static void
ibar_interface_init (gpointer         g_iface,
                    gpointer         iface_data)
{
        MamanIbarClass *klass = (MamanIbarClass *)g_iface;
        klass->do_another_action = (void (*) (MamanIbar *self))ibar_do_another_action;
}


static void ibaz_do_action (MamanBar *self)
{
        g_print ("Bar implementation of IBaz interface Action: 0x%x.\n", self->instance_member);
}

static void
ibaz_interface_init (gpointer         g_iface,
                    gpointer         iface_data)
{
        MamanIbazClass *klass = (MamanIbazClass *)g_iface;
        klass->do_action = (void (*) (MamanIbaz *self))ibaz_do_action;
}


static void
bar_instance_init (GTypeInstance   *instance,
                   gpointer         g_class)
{
        MamanBar *self = (MamanBar *)instance;
        self->instance_member = 0x666;
}


GType 
maman_bar_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanBarClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        NULL,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanBar),
                        0,      /* n_preallocs */
                        bar_instance_init    /* instance_init */
                };
                static const GInterfaceInfo ibar_info = {
                        (GInterfaceInitFunc) ibar_interface_init,   /* interface_init */
                        NULL,                                       /* interface_finalize */
                        NULL                                        /* interface_data */
                };
                static const GInterfaceInfo ibaz_info = {
                        (GInterfaceInitFunc) ibaz_interface_init,   /* interface_init */
                        NULL,                                       /* interface_finalize */
                        NULL                                        /* interface_data */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanBarType",
                                               &info, 0);
                g_type_add_interface_static (type,
                                             MAMAN_TYPE_IBAZ,
                                             &ibaz_info);
                g_type_add_interface_static (type,
                                             MAMAN_TYPE_IBAR,
                                             &ibar_info);
        }
        return type;
}

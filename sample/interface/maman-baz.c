/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */

#include "maman-baz.h"
#include "maman-ibaz.h"

static void baz_do_action (MamanBaz *self)
{
        g_print ("Baz implementation of IBaz interface Action: 0x%x.\n", self->instance_member);
}

static void
baz_interface_init (gpointer         g_iface,
                    gpointer         iface_data)
{
        MamanIbazClass *klass = (MamanIbazClass *)g_iface;
        klass->do_action = (void (*) (MamanIbaz *self))baz_do_action;
}

static void
baz_instance_init (GTypeInstance   *instance,
                   gpointer         g_class)
{
        MamanBaz *self = (MamanBaz *)instance;
        self->instance_member = 0xdeadbeaf;
}


GType 
maman_baz_get_type (void)
{
        static GType type = 0;
        if (type == 0) {
                static const GTypeInfo info = {
                        sizeof (MamanBazClass),
                        NULL,   /* base_init */
                        NULL,   /* base_finalize */
                        NULL,   /* class_init */
                        NULL,   /* class_finalize */
                        NULL,   /* class_data */
                        sizeof (MamanBaz),
                        0,      /* n_preallocs */
                        baz_instance_init    /* instance_init */
                };
                static const GInterfaceInfo ibaz_info = {
                        (GInterfaceInitFunc) baz_interface_init,    /* interface_init */
                        NULL,                                       /* interface_finalize */
                        NULL                                        /* interface_data */
                };
                type = g_type_register_static (G_TYPE_OBJECT,
                                               "MamanBazType",
                                               &info, 0);
                g_type_add_interface_static (type,
                                             MAMAN_TYPE_IBAZ,
                                             &ibaz_info);
        }
        return type;
}

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/**
 *
 * MamanIBar is an interface implemented in maman-bar.h/c
 *
 *
 */

#ifndef MAMAN_IBAR_H
#define MAMAN_IBAR_H

#include <glib-object.h>

#define MAMAN_TYPE_IBAR             (maman_ibar_get_type ())
#define MAMAN_IBAR(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAMAN_TYPE_IBAR, MamanIbar))
#define MAMAN_IBAR_CLASS(vtable)    (G_TYPE_CHECK_CLASS_CAST ((vtable), MAMAN_TYPE_IBAR, MamanIbarClass))
#define MAMAN_IS_IBAR(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAMAN_TYPE_IBAR))
#define MAMAN_IS_IBAR_CLASS(vtable) (G_TYPE_CHECK_CLASS_TYPE ((vtable), MAMAN_TYPE_IBAR))
#define MAMAN_IBAR_GET_CLASS(inst)  (G_TYPE_INSTANCE_GET_INTERFACE ((inst), MAMAN_TYPE_IBAR, MamanIbarClass))


typedef struct _MamanIbar MamanIbar; /* dummy object */
typedef struct _MamanIbarClass MamanIbarClass;

struct _MamanIbarClass {
        GTypeInterface parent;

        void (*do_another_action) (MamanIbar *self);
};

GType maman_ibar_get_type (void);

void maman_ibar_do_another_action (MamanIbar *self);

#endif //MAMAN_IBAR_H










